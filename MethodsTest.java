public class MethodsTest{
	
	public static void main(String[] args){
		
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		methodTwoInputNoReturn(5, 6.7);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double imtired = sumSquareRoot(9,5);
		System.out.println(imtired);
		
		String s1 = "java";
		String s2 = "programming";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int gaming){
		System.out.println("Inside the method one input no return");
		gaming = gaming-5;
		System.out.println(gaming);
	}
	
	public static void methodTwoInputNoReturn(int aha, double hah){
		System.out.println(aha);
		System.out.println(hah);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int x, int y){
		double z = x+y;
		z = Math.sqrt(z);
		return z;
	}
}