import java.util.Scanner;
public class PartThree{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Calculator calc = new Calculator();
		
		System.out.println("enter first number (int)");
		int x = scan.nextInt();
		System.out.println("enter second number (int)");
		int y = scan.nextInt();
		
		System.out.println(Calculator.add(x,y));
		System.out.println(Calculator.subtract(x,y));
		System.out.println(calc.multiply(x,y));
		System.out.println(calc.divide(x,y));
		
	}
}